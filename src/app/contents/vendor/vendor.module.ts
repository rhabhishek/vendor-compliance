import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, Router, NavigationStart, NavigationEnd, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';

import { SummaryComponent } from './summary/summary.component';
import { DetailsComponent } from './details/details.component';
import { TabInsuranceComponent } from './summary/tab-insurance/tab-insurance.component';
import { TabLocationsComponent } from './summary/tab-locations/tab-locations.component';
import { TabDocumentsComponent } from './summary/tab-documents/tab-documents.component';

export const routes: Routes = [
  { path: 'agents/:agent/summary', component: SummaryComponent },
  { path: 'agents/:agent/details', component: DetailsComponent }
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyArVpxaMB8TqOeoaKqD2Mzcep1gyv8UgfI'
    }),
    RouterModule.forChild(routes)
  ],
  declarations: [SummaryComponent, DetailsComponent, TabInsuranceComponent, TabLocationsComponent, TabDocumentsComponent]
})
export class VendorModule { }

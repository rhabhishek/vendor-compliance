import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {
    companyInfo = true;
    vendorDetails = false;

  constructor() {
  }

  ngOnInit() {
  }

  shownHide(tab) {
    if (tab === 'company-info') {
      this.companyInfo = true;
      this.vendorDetails = false;
    } else {
      this.companyInfo = false;
      this.vendorDetails = true;
    }
  }

}

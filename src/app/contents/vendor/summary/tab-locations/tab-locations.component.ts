import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../../../services/data.service';

@Component({
  selector: 'app-tab-locations',
  templateUrl: './tab-locations.component.html',
  styleUrls: ['../tabs-common.less', './tab-locations.component.less']
})
export class TabLocationsComponent implements OnInit {
  lat: number = 51.678418;
  lng: number = 7.809007;
  @Input('locations') locations;

  constructor(private dataService: DataService) {
   }

  ngOnInit() {
  }

  updateLocation(address){
    //map Docs: https://angular-maps.com/api-docs/agm-core/components/AgmMap.html#zoom
    //use geoCoding api and resolve the address into lat and long
    //https://developers.google.com/maps/documentation/geocoding/intro
    this.dataService.getLocation(address)
    .subscribe((data)=>{
      var resolvedLocation = data['results'];
      this.lat = resolvedLocation[0].geometry.location.lat;
      this.lng = resolvedLocation[0].geometry.location.lng;
    })
  }

}

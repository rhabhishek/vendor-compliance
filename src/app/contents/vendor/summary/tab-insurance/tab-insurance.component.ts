import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';

@Component({
  selector: 'app-tab-insurance',
  templateUrl: './tab-insurance.component.html',
  styleUrls: ['../tabs-common.less', './tab-insurance.component.less']
})
export class TabInsuranceComponent implements OnInit {
  documents=[];
  constructor(private dataService: DataService) {
    this.documents = dataService.getDocuments();
  }

  ngOnInit() {
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  data = [
    {
      "_id": "5af0b64717cfd6726e6296c7",
      "vendorID": 1285,
      "name": "Roboid",
      "address": "157 Crescent Street",
      "city": "Barclay",
      "state": "Marshall Islands",
      "zip": 1417,
      "phone": "+1 (844) 550-3574",
      "contact": "Nikki Gordon",
      "dba": "ID8603",
      "status": "inactive",
      "platinum": false,
      "pending" : true,
      "compliancePercent" : 34,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 11210",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        },
        {
          "name" : "Brooklyn Warehouse",
          "address" : "2 Northside Dr NW, Atlanta, GA 30313",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        },
        {
          "name" : "Brooklyn Warehouse",
          "address" : "260 Brush Mountain Rd, Altoona, PA 16601",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        },
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3045 Brooklyn Road, Brooklyn, NY, 11210",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b647a5ba48cb546ef165",
      "vendorID": 1995,
      "name": "Primordia",
      "address": "138 Seagate Terrace",
      "city": "Boling",
      "state": "Arkansas",
      "zip": 6132,
      "phone": "+1 (925) 583-3129",
      "contact": "Krystal Wilcox",
      "dba": "ID4432",
      "status": "active",
      "platinum": true,
      "pending" : false,
      "compliancePercent" : 96,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b6472f9f3dd231477d75",
      "vendorID": 1762,
      "name": "Portico",
      "address": "382 Rutledge Street",
      "city": "Loomis",
      "state": "Georgia",
      "zip": 4949,
      "phone": "+1 (826) 597-2963",
      "contact": "Bridget Gray",
      "dba": "ID4307",
      "status": "pending",
      "platinum": false,
      "pending" : false,
      "compliancePercent" : 53,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b6474210de4ad2188b93",
      "vendorID": 1177,
      "name": "Zogak",
      "address": "131 Main Street",
      "city": "Glendale",
      "state": "Virginia",
      "zip": 8967,
      "phone": "+1 (978) 421-3907",
      "contact": "Sara Wells",
      "dba": "ID9563",
      "status": "inactive",
      "platinum": true,
      "pending" : false,
      "compliancePercent" : 100,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b6475ec3aa020940c164",
      "vendorID": 1924,
      "name": "Liquidoc",
      "address": "755 Creamer Street",
      "city": "Glenbrook",
      "state": "North Carolina",
      "zip": 5534,
      "phone": "+1 (978) 490-2240",
      "contact": "Marsha Castillo",
      "dba": "ID9417",
      "status": "inactive",
      "platinum": false,
      "pending" : false,
      "compliancePercent" : 66,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b6472b884f51f7d45380",
      "vendorID": 1998,
      "name": "Daido",
      "address": "718 Quentin Road",
      "city": "Joes",
      "state": "Alaska",
      "zip": 3160,
      "phone": "+1 (877) 505-2163",
      "contact": "Lowe Faulkner",
      "dba": "ID7082",
      "status": "active",
      "platinum": false,
      "pending" : false,
      "compliancePercent" : 34,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    },
    {
      "_id": "5af0b647a8192ed2f49cc5a4",
      "vendorID": 1592,
      "name": "Ultrasure",
      "address": "600 Halsey Street",
      "city": "Torboy",
      "state": "Nebraska",
      "zip": 5148,
      "phone": "+1 (982) 409-3067",
      "contact": "Barry Mcintosh",
      "dba": "ID1774",
      "status": "pending",
      "platinum": false,
      "pending" : true,
      "compliancePercent" : 25,
      "storage lots" : 13,
      "locations" : [
        {
          "name" : "Brooklyn Warehouse",
          "address" : "3040 Brooklyn Road, Brooklyn, NY, 41201",
          "contact" : "+1 344 415 0011",
          "vehicles" : [
            {
              "make" : "Ford",
              "model" : "F150",
              "VIN" : "456123465",
              "registration" : "DK4512SER"
            }
          ]
        }
      ],
      "owner" : {
        "name": "John Doe",
        "contact": "+1 214 000 0120",
        "email" : "john.doe@example.com"
      },

    }
  ];

  documents = [
  {
    "_id": "5af1c204b37c1b95698e4487",
    "url": "http://placehold.it/32x32",
    "createdAt": "Tue Feb 21 2017 11:38:15 GMT-0500 (Eastern Standard Time)",
    "name": "jan 2018_invoice.docx",
    "type": "invoice",
    "expiring" : false
  },
  {
    "_id": "5af1c2041918ab33d325781d",
    "url": "http://placehold.it/32x32",
    "createdAt": "Mon Mar 28 2016 02:25:10 GMT-0400 (Eastern Daylight Time)",
    "name": "june18_invoice.docx",
    "type": "invoice",
    "expiring" : false
  },
  {
    "_id": "5af1c20427385edeac10dc23",
    "url": "http://placehold.it/32x32",
    "createdAt": null,
    "name": "2018_full_coverage.docx",
    "type": "insurance",
    "expiring" : true
  },
  {
    "_id": "5af1c204010daddc4af785f6",
    "url": "http://placehold.it/32x32",
    "createdAt": "Fri Nov 11 2016 02:16:10 GMT-0500 (Eastern Standard Time)",
    "name": "auto_liab_2018.pdf",
    "type": "insurance",
    "expiring" : false
  },
  {
    "_id": "5af1c204e92ca10b77002718",
    "url": "http://placehold.it/32x32",
    "createdAt": null,
    "name": "2018_chevy_registration.pdf",
    "type": "registration",
    "expiring" : true
  }
]

  constructor(private http: HttpClient) { };

  getData(param?: number): any{
    if (param) {
      return this.data.find((elem)=>{
        return elem.vendorID == param;
      })
    }else return this.data;
  }

  getDocuments(){
    return this.documents;
  }

  getLocation(address){
    return this.http.get("https://maps.googleapis.com/maps/api/geocode/json?address="+ address+"&key=AIzaSyArVpxaMB8TqOeoaKqD2Mzcep1gyv8UgfI");
  }

}

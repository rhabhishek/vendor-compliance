<!DOCTYPE html>

<html lang="en">
<head>
  <title>Agent Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap4.min.css">
  <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
  <script type="text/javascript" src="js/loader.js"></script>
  <link rel="stylesheet" href="css/site.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/dataTables.bootstrap4.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  
</head>
<body>

<div class="container-fluid header">
<div class="container">
		<h1 class="float-left mb-2"><img src="images/admin-logo.png" /></h1>
		<div class="searchWidget float-left ml-4 mt-2">
			<input type="text" class="rounded" placeholder="Search Agent">
		</div>
		<nav class="float-right mt-3">
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="#">Contact Us</a></li>
				<li class="user"><a href="#" onclick="myFunction()" class="dropbtn">Venkatesh</a></li>
				
			</ul>
			<div id="myDropdown" class="dropdown-content">
				<a href="#profile">Profile</a>
				<a href="#logout">Log Out</a>
			</div>
		</nav>
</div>
</div>


<div class="container">
<div class="col-md-12 row no-gutters">
	<div class="rounded widget col-md-12 mt-4">
		<h1 class="widget_header">Agent Name</h1>
		
		<div class="agent_details widget_body">
			<div class="col-md-12 float-left mb-3" style="border-bottom: 1px solid #ccc;">
			<ul class="nav nav-tabs mt-4 col-md-8 float-left" role="tablist" style="border-bottom: none;">
				<li class="nav-item ml-4">
				  <a class="nav-link" href="agent_details.html">Agent</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="agent_summary.html">Summary</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link active" href="agent_documents.html">Document</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#Location" id="tab_location">Location</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#Employee">Employee</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#Insurance">Insurance</a>
				</li>
		    </ul>
			<div class="float-left col-md-4 mt-4">
				<label class="col-md-4 float-left">Compliance</label>
				<div class="progress col-md-6 float-left" style="padding-left: 0px; padding-right: 0px;">
					<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:40%"></div>
					
				</div>
				<span class="col-md-1 float-left"><b>40%</b></span>
			</div>
			</div>
			<!-- Documents Tab -->
				<div id="Document" class="container tab-pane float-left">
					<div class="col-md-12">
						<div class="form-group">
						  <label>
							<h3 class="h5">Document Upload</h3>
						  </label>
						  <div class="row col-md-12 mt-3">
							<div class="col-md-2">Type Of Document</div>
							<div class="col-md-3" style="bottom:6px;">
							  <select class="form-control" >
							    <option>Please Select Option</option>
								<option>Collection License</option>
								<option>Agreement/Contract</option>
								<option>CARS Certification</option>
								<option>Certificate of Insurance (COI)</option>
								
							  </select>
							</div>

						  </div>

						</div>
				    </div>
					
					<div class="col-md-12 text-left">

        <div class="table-bordered">
          <table class="table table-striped">
            <thead>
              <tr>
                <th></th>
                <th>Document Type </th>
                <th>File </th>
                <th>Upload Date</th>
				<th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td data-toggle="modal" data-target="#myModal">
                  <img src="images/edit_icon.png" />
                </td>
                <td>Collection License</td>
                <td>XYZ Collection</td>
                <td>5/18/2016</td>
				<td onclick="alertFn()"><i class="fa fa-times iconSet"  aria-hidden="true"></i></td>

              </tr>
              <tr>
                <td data-toggle="modal" data-target="#myModal2">
                  <img src="images/edit_icon.png" />
                </td>
                <td>Agreement/Contract</td>
                <td>XYZ Respossession</td>
                <td>2/3/2018</td>
				<td onclick="alertFn()"><i class="fa fa-times iconSet"  aria-hidden="true"></i></td>	
              </tr>
			  <tr>
                <td data-toggle="modal" data-target="#myModal3">
                  <img src="images/edit_icon.png" />
                </td>
                <td>CARS Certification</td>
                <td>XYZ Respossession</td>
                <td>4/13/2018</td>
				<td onclick="alertFn()"><i class="fa fa-times iconSet"  aria-hidden="true"></i></td>

              </tr>
			  <tr>
                <td data-toggle="modal" data-target="#myModal4">
                  <img src="images/edit_icon.png" />
                </td>
                <td>Certificate of Insurance (COI)</td>
                <td>XYZ Respossession</td>
                <td>7/27/2018</td>
				<td onclick="alertFn()"><i class="fa fa-times iconSet"  aria-hidden="true"></i></td>

              </tr>


            </tbody>

          </table>

        </div>
		<div class="col-md-3 mx-auto mb-3">
        <ul class="pagination mt-3">
          <li class="page-item">
            <a class="page-link" href="#">Previous</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">1</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">2</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">3</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">Next</a>
          </li>
        </ul>
		</div>
      </div>
	  
	  
	  
	  <div class="container">


      <!-- The Modal 1-->
      <div class="modal" id="myModal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div style="background: #F3F9F7;border: 5px solid #337ab7;" class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <span style="margin-left: 28%;font-weight: 600;font-size: 18px;">Document Information and Upload File</span>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>

            <!-- Modal body -->
            <div class="modal-body col-md-12">
              <div class="row ">
                <div style="padding: 10px;">
                  <label style="padding-right: 22px;" for="usr">Type of Document</label>
                  <input type="text" id="usr" value="Collection License">
                </div>
                <div style="padding: 12px;">
                  <label style="padding-right: 22px;padding-left:27px" for="pwd"> Issue Date</label>
                  <input type="date" >
                </div>
              </div>
              <div class="row">
                <div style="padding: 32px;">
                  <label style="padding-right: 22px;" for="usr">Expiration Date</label>
                  <input type="date" >
                </div>
                <div style="padding: 32px;padding-left:20px;">
                  <label style="padding-right: 19px;" for="pwd">Issue State</label>
                  <input type="text" >
                </div>
              </div>
             <div class="form-group row col-md-5" style="float:initial;margin-left:auto;margin-right:auto;"> 
                <label>
                  <input type="file" class="form-control">
                </label>
              </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" style="float:initial;margin-left:auto;margin-right:auto;" class="btn btn-success">Submit</button>
            </div>

          </div>
        </div>
      </div>

    </div>
	
	<div class="container">


      <!-- The Modal 2 -->
      <div class="modal" id="myModal2">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div style="background: #F3F9F7;border: 5px solid #337ab7;" class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <span style="margin-left: 28%;font-weight: 600;font-size: 18px;">Document Information and Upload File</span>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>

            <!-- Modal body -->
            <div class="modal-body col-md-12">
              <div class="row ">
                <div style="padding: 10px;">
                  <label style="padding-right: 22px;" for="usr">Type of Document</label>
                  <input type="text" id="usr2" value="Agreement/Contract">
                </div>
                <div style="padding: 12px;">
                  <label style="padding-right: 22px;padding-left:27px" for="pwd">Agent Name</label>
                  <input type="text" >
                </div>
              </div>
              <div class="row">
                <div style="padding: 46px;">
                  <label style="padding-right: 22px;" for="usr">Owner Name</label>
                  <input type="text" >
                </div>
                <div style="padding: 44px;padding-left:10px;">
                  <label style="padding-right: 19px;" for="pwd">Date Signed</label>
                  <input type="date" >
                </div>
              </div>
			  
              <div class="form-group row col-md-5" style="float:initial;margin-left:auto;margin-right:auto;">
                <label>
                  <input type="file" class="form-control">
                </label>
              </div>
			  
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" style="float:initial;margin-left:auto;margin-right:auto;" class="btn btn-success">Submit</button>
            </div>

          </div>
        </div>
      </div>

    </div>
	
	<div class="container">


      <!-- The Modal 3-->
      <div class="modal" id="myModal3">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div style="background: #F3F9F7;border: 5px solid #337ab7;" class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <span style="margin-left: 28%;font-weight: 600;font-size: 18px;">Document Information and Upload File</span>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>

            <!-- Modal body -->
            <div class="modal-body col-md-12">
              <div class="row ">
                <div style="padding: 10px;">
                  <label style="padding-right: 22px;" for="usr">Type of Document</label>
                  <input type="text" id="usr3" value="CARS Certification">
                </div>
                <div style="padding: 12px;">
                  <label style="padding-right: 22px;padding-left:27px" for="pwd">Certificate ID</label>
                  <input type="text" >
                </div>
              </div>
              <div class="row">
                <div style="padding: 25px;">
                  <label style="padding-right: 22px;" for="usr">Name of Person</label>
                  <input type="text" >
                </div>
                <div style="padding: 25px;padding-left:24px;">
                  <label style="padding-right: 19px;" for="pwd">Certified Date</label>
                  <input type="date" >
                </div>
              </div>
			  
			  <div class="form-group row col-md-5" style="float:initial;margin-left:auto;margin-right:auto;">
                <label>
                  <input type="file" class="form-control">
                </label>
              </div>
               
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" style="float:initial;margin-left:auto;margin-right:auto;" class="btn btn-success">Submit</button>
            </div>

          </div>
        </div>
      </div>

    </div>
	
	<div class="container">


      <!-- The Modal 4 -->
      <div class="modal" id="myModal4">
        <div class="modal-dialog modal-dialog-centered modal-lg">
          <div style="background: #F3F9F7;border: 5px solid #337ab7;" class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <span style="margin-left: 28%;font-weight: 600;font-size: 18px;">Document Information and Upload File</span>
              <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>

            <!-- Modal body -->
            <div class="modal-body col-md-12">
              <div class="row ">
                <div style="padding: 10px;">
                  <label style="padding-right: 22px;" for="usr">Type of Document</label>
                  <input type="text" id="usr4" value="Certificate of Insurance (COI)">
                </div>
                <div style="padding: 12px;">
                  <label style="padding-right: 22px;padding-left:27px" for="pwd">Effective Date</label>
                  <input type="date" >
                </div>
              </div>
              <div class="row">
                <div style="padding: 31px;">
                  <label style="padding-right: 22px;" for="usr">Expiration Date</label>
                  <input type="date" >
                </div>
                <div style="padding: 31px;padding-left:26px;">
                  <label style="padding-right: 19px;" for="pwd">Liability Limit</label>
                  <input type="text" >
                </div>
              </div>
			  
              <div class="form-group row col-md-5" style="float:initial;margin-left:auto;margin-right:auto;">
                <label>
                  <input type="file" class="form-control">
                </label>
              </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" style="float:initial;margin-left:auto;margin-right:auto;" class="btn btn-success">Submit</button>
            </div>

          </div>
        </div>
      </div>

    </div>
				</div>
				<!-- Documents Tab -->
		</div>
	</div>
</div>
</div>



<div class="container-fluid float-left mt-3 mb-3 footer">
<p class="text-center">© Copyright 2018 PAR Vision. All rights reserved.</p>
</div>




<script>
$(document).ready(function(){
    $("select").change(function(){
    var opval = $(this).val();
    if(opval=="Collection License"){
	document.getElementById('usr').value=opval;
        $('#myModal').modal("show");
    }
	if(opval=="Agreement/Contract"){
	document.getElementById('usr2').value=opval;
        $('#myModal2').modal("show");
    }
	
	if(opval=="Certificate of Insurance (COI)"){
	document.getElementById('usr4').value=opval;
        $('#myModal4').modal("show");
    }
	if(opval=="CARS Certification"){
	document.getElementById('usr3').value=opval;
        $('#myModal3').modal("show");
    }
	if(opval=="Commercial Liability"){
	document.getElementById('usr3').value=opval;
        $('#myModal_insurance').modal("show");
    }
	if(opval=="Site Inspection Report"){
	document.getElementById('usr').value=opval;
        $('#myModal_location').modal("show");
    }
	if(opval=="Garage Keepers Policy/Insurance"){
	document.getElementById('usr2').value=opval;
        $('#myModal_location2').modal("show");
    }
	
    });
});
</script>

</body>
</html>